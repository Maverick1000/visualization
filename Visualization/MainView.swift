//
//  MainView.swift
//  Visualization
//
//  Created by PWalker on 1/20/16.
//  Copyright © 2016 Paul Walker. All rights reserved.
//

import UIKit

class MainView : UIViewController,
        MainNavigationInterface,
        MainPresenterToViewInterface
        {
        // MARK: - VIPER Stack
        lazy var presenter : MainViewToPresenterInterface = MainPresenter()
        
        // MARK: - Instance Variables
        
        // MARK: - Outlets
        
        // MARK: - Operational
        
        @IBAction func userTouchButton(_ sender: AnyObject) {
                presenter.UserClickButton("Test");
        }
        // MARK: - View Interface
        
}
