//
//  MainPresenter.swift
//  Visualization
//
//  Created by PWalker on 1/20/16.
//  Copyright © 2016 Paul Walker. All rights reserved.
//

import Foundation
import MediaPlayer

class MainPresenter : NSObject,
MainInteractorToPresenterInterface,
MainViewToPresenterInterface,
MainWireframeToPresenterInterface
{
	// MARK: - VIPER Stack
	lazy var interactor : MainPresenterToInteractorInterface = MainInteractor()
	lazy var view : MainPresenterToViewInterface = MainView()
	lazy var wireframe : MainPresesnterToWireframeInterface = MainWireframe()
	
	// MARK: - Instance Variables
	var audioSession: AVAudioSession = AVAudioSession.sharedInstance()
	var audioPlayer: AVAudioPlayer?
	
	// MARK: - OperationaudioFileURL:al
	
	// MARK: - Interactor Output
	
	// MARK: - Presenter Interface
	func UserClickButton(_ string: String) {
		print(string) ;
		
		let mediaQuery: MPMediaQuery = MPMediaQuery()
		if let items = mediaQuery.items {
			for song in items {
				print(String(describing: song.value(forProperty: MPMediaItemPropertyTitle)))
			}
			
		}
		//do {
			try! audioSession.setCategory(AVAudioSessionCategoryPlayback)
		//}
		//catch{
                  //      print(error)
		//}
		
		let test = mediaQuery.items![0]
		let string = test.value(forProperty: MPMediaItemPropertyAssetURL) as! URL
		audioPlayer = try! AVAudioPlayer(contentsOf: string)
		audioPlayer?.prepareToPlay()
		audioPlayer?.play()
		
		
		// {
		
//		}
//
//		catch {
//
//		}
	}
	
// MARK: - Routing
	
}
