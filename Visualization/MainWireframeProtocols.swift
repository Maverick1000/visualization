//
//  MainWireframeProtocols.swift
//  Visualization
//
//  Created by PWalker on 1/20/16.
//  Copyright © 2016 Paul Walker. All rights reserved.
//

// VIPER Module Constants
// Uncomment to utilize a navigation contoller from storyboard
//let kMainNavigationControllerIdentifier = "MainNavigationController"
let kMainStoryboardIdentifier = "Main"
let kMainViewIdentifier = "MainView"

// VIPER Interface to the Module
protocol MainDelegate : class {
        
}

// Interface Abstraction for working with the VIPER Module
protocol MainModuleInterface : class {
        
}

// VIPER Interface for communication from Presenter -> Wireframe
protocol MainPresesnterToWireframeInterface : class {
        
}