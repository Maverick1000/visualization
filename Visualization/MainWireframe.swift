//
//  MainWireframe.swift
//  Visualization
//
//  Created by PWalker on 1/20/16.
//  Copyright © 2016 Paul Walker. All rights reserved.
//

import UIKit
import AVFoundation

class MainWireframe: NSObject,
MainModuleInterface,
MainPresesnterToWireframeInterface
{
	// MARK: - VIPER Stack
	lazy var moduleInteractor = MainInteractor()
	// Uncomment the comment lines and delete the moduleView line default code to use a navigationController from storyboard
	/*
	 lazy var moduleNavigationController: UINavigationController = {
	 let sb = MainWireframe.storyboard()
	 let v = sb.instantiateViewControllerWithIdentifier(kMainNavigationControllerIdentifier) as! UINavigationController
	 return v
	 }()
	 */
	lazy var modulePresenter = MainPresenter()
	/*
	 lazy var moduleView: MainView = {
	 return self.moduleNavigationController.viewControllers[0] as! MainView
	 }()
	 */
	
	lazy var moduleView: MainView = {
		let sb = MainWireframe.storyboard()
		let vc = sb.instantiateViewController(withIdentifier: kMainViewIdentifier) as! MainView
		return vc
	}()
	lazy var presenter : MainWireframeToPresenterInterface = self.modulePresenter
	lazy var view : MainNavigationInterface = self.moduleView
	
	// MARK: - Instance Variables
	var delegate: MainDelegate?

	// MARK: - Initialization
	override init() {
		super.init()
		
		let i = moduleInteractor
		let p = modulePresenter
		let v = moduleView
		
		i.presenter = p
		
		p.interactor = i
		p.view = v
		p.wireframe = self
		
		v.presenter = p
		
		presenter = p
	}
	
	class func storyboard() -> UIStoryboard {
		return UIStoryboard(name: kMainStoryboardIdentifier, bundle: Bundle(for: MainWireframe.self))
	}
	
	// MARK: - Operational
	
	// MARK: - Module Interface
	
	// MARK: - Wireframe Interface
	
}
