//
//  MainPresenterProtocols.swift
//  Visualization
//
//  Created by PWalker on 1/20/16.
//  Copyright © 2016 Paul Walker. All rights reserved.
//

// VIPER Interface for communication from Interactor -> Presenter
protocol MainInteractorToPresenterInterface : class {
        
}

// VIPER Interface for communication from View -> Presenter
protocol MainViewToPresenterInterface : class {
        func UserClickButton(_ string:String)
}

// VIPER Interface for communication from Wireframe -> Presenter
protocol MainWireframeToPresenterInterface : class {
        
}
