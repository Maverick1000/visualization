//
//  MainViewProtocols.swift
//  Visualization
//
//  Created by PWalker on 1/20/16.
//  Copyright © 2016 Paul Walker. All rights reserved.
//

// VIPER Interface for manipulating the navigation of the view
protocol MainNavigationInterface : class {
        
}

// VIPER Interface for communication from Presenter -> View
protocol MainPresenterToViewInterface : class {
        
}